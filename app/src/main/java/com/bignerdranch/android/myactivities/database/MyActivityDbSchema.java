package com.bignerdranch.android.myactivities.database;

public class MyActivityDbSchema
{
  public static final class MyActivityTable
  {
    public static final String NAME = "my_activities";
    
    public static final class Cols
    {
      public static final String COMMENT = "comment";
      public static final String DATE = "date";
      public static final String DURATION = "duration";
      public static final String LATITUDE = "latitude";
      public static final String LONGITUDE = "longitude";
      public static final String SOLVED = "solved";
      public static final String SUSCPECT = "suspect";
      public static final String TITLE = "title";
      public static final String TYPE = "type";
      public static final String UUID = "uuid";
    }
  }
  
  public static final class UserProfile
  {
    public static final String NAME = "user_profile";
    
    public static final class Cols
    {
      public static final String COMMENT = "comment";
      public static final String EMAIL = "email";
      public static final String GENDER = "gender";
      public static final String IDNUM = "idnum";
      public static final String NAME = "name";
      public static final String UUID = "uuid";
    }
  }
}