package com.bignerdranch.android.myactivities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

public class PictureUtils
{
  public static Bitmap getScaledBitmap(String path, int destWidth, int destHeight)
  {
    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inJustDecodeBounds = true;
    BitmapFactory.decodeFile(path, options);
    float srcWidth = options.outWidth;
    float srcHeight = options.outHeight;

    int inSampleSize = 1;
    if ((srcHeight > destHeight) || (srcWidth > destWidth)) {
      if (srcWidth <= srcHeight)
      {
        inSampleSize = Math.round(srcHeight / destHeight);
      }
      else
      {
        inSampleSize = Math.round(srcWidth / destWidth);
      }
    }
    options = new BitmapFactory.Options();
    options.inSampleSize = inSampleSize;

    return BitmapFactory.decodeFile(path, options);
  }
  
  public static Bitmap getScaledBitmap(String paramString, Activity paramActivity)
  {
    Point localPoint = new Point();
    paramActivity.getWindowManager().getDefaultDisplay().getSize(localPoint);
    return getScaledBitmap(paramString, localPoint.x, localPoint.y);
  }
}