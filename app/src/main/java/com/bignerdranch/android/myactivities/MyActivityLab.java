package com.bignerdranch.android.myactivities;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import com.bignerdranch.android.myactivities.database.MyActivityBaseHelper;
import com.bignerdranch.android.myactivities.database.MyActivityCursorWrapper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MyActivityLab
{
  private static MyActivityLab sMyActivityLab;
  private Context mContext;
  private SQLiteDatabase mDatabase;
  
  private MyActivityLab(Context paramContext)
  {
    this.mContext = paramContext.getApplicationContext();
    this.mDatabase = new MyActivityBaseHelper(this.mContext).getWritableDatabase();
  }
  
  private static ContentValues getContentValues(MyActivity paramMyMyActivity)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("uuid", paramMyMyActivity.getId().toString());
    localContentValues.put("title", paramMyMyActivity.getTitle());
    localContentValues.put("date", Long.valueOf(paramMyMyActivity.getDate().getTime()));
    if (paramMyMyActivity.isSolved()) {}
    for (int i = 1;; i = 0)
    {
      localContentValues.put("solved", Integer.valueOf(i));
      localContentValues.put("suspect", paramMyMyActivity.getSuspect());
      localContentValues.put("type", paramMyMyActivity.getType());
      localContentValues.put("comment", paramMyMyActivity.getComment());
      localContentValues.put("duration", paramMyMyActivity.getDuration());
      localContentValues.put("latitude", paramMyMyActivity.getLatitude());
      localContentValues.put("longitude", paramMyMyActivity.getLongitude());
      return localContentValues;
    }
  }
  
  public static MyActivityLab getMyActivity(Context context)
  {
    if (sMyActivityLab == null) {
      sMyActivityLab = new MyActivityLab(context);
    }
    return sMyActivityLab;
  }
  
  private static ContentValues getUpContentValues(UserProfile paramUserProfile)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("uuid", paramUserProfile.getmId().toString());
    localContentValues.put("name", paramUserProfile.getmName());
    localContentValues.put("email", paramUserProfile.getmEmail());
    localContentValues.put("comment", paramUserProfile.getmComment());
    localContentValues.put("gender", paramUserProfile.getmGender());
    localContentValues.put("idnum", paramUserProfile.getmIdnum());
    return localContentValues;
  }
  
  private MyActivityCursorWrapper queryActivities(String paramString, String[] paramArrayOfString)
  {
    return new MyActivityCursorWrapper(this.mDatabase.query("my_activities", null, paramString, paramArrayOfString, null, null, null));
  }
  
  private MyActivityCursorWrapper queryUps(String paramString, String[] paramArrayOfString)
  {
    return new MyActivityCursorWrapper(this.mDatabase.query("user_profile", null, paramString, paramArrayOfString, null, null, null));
  }
  
  public void addMyActivity(MyActivity c)
  {
    ContentValues values = getContentValues(c);
    this.mDatabase.insert("my_activities", null, values);
  }
  
  public void addUserProfile(UserProfile profile)
  {
    ContentValues values = getUpContentValues(profile);
    this.mDatabase.insert("user_profile", null, values);
  }
  
  public void deleteMyActivity(MyActivity paramMyActivity)
  {
    this.mDatabase.delete("my_activities", "uuid = ?", new String[] { paramMyActivity.getId().toString() });
  }
  
  public MyActivity getMyActivity(UUID id)
  {
    MyActivityCursorWrapper cursor = queryActivities("uuid = ?", new String[] { id.toString() });
    try
    {
      if (cursor.getCount() == 0) {
        return null;
      }
      cursor.moveToFirst();
      return cursor.getMyActivities();
    }
    finally
    {
      cursor.close();
    }
  }
  
  public List<MyActivity> getMyActivities()
  {
    List<MyActivity> activities = new ArrayList<>();
    MyActivityCursorWrapper cursor = queryActivities(null, null);
    try
    {
      cursor.moveToFirst();
      while (!cursor.isAfterLast())
      {
        activities.add(cursor.getMyActivities());
        cursor.moveToNext();
      }
    }
    finally
    {
      cursor.close();
    }
    return activities;
  }
  
  public File getPhotoFile(MyActivity myActivity)
  {
    File externalFilesDir = this.mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    if (externalFilesDir == null) {
      return null;
    }
    return new File(externalFilesDir, myActivity.getPhotoFilename());
  }
  
  public UserProfile getUserProfile(UUID id)
  {
    MyActivityCursorWrapper cursor = queryUps("uuid = ?", new String[] { id.toString() });
    try
    {
      if (cursor.getCount() == 0) {
        return null;
      }
      cursor.moveToFirst();
      return cursor.getUserProfile();
    }
    finally
    {
      cursor.close();
    }
  }
  
  public void updateMyActivity(MyActivity myActivity)
  {
    String uuidString = myActivity.getId().toString();
    ContentValues values = getContentValues(myActivity);
    this.mDatabase.update("my_activities", values, "uuid = ?", new String[] { uuidString });
  }
  
  public void updateUserProfile(UserProfile myUserProfile)
  {
    String uuidString = myUserProfile.getmId().toString();
    ContentValues values = getUpContentValues(myUserProfile);
    this.mDatabase.update("user_profile", values, "uuid = ?", new String[] { uuidString });
  }
}