package com.bignerdranch.android.myactivities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public abstract class SingleFragmentActivity
  extends AppCompatActivity
{
  protected abstract Fragment createFragment();
  
  public void onCreate(Bundle bundle)
  {
    super.onCreate(bundle);
    setContentView(R.layout.fragment_my_activity);
    FragmentManager fm = getSupportFragmentManager();
    Fragment fragment = fm.findFragmentById(R.id.fragment_container);
    if (fragment == null)
    {
      fragment = createFragment();
      bundle.beginTransaction().add(R.id.fragment_container, fragment).commit();
    }
  }
}