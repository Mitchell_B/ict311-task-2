package com.bignerdranch.android.myactivities.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyActivityBaseHelper
  extends SQLiteOpenHelper
{
  private static final String DATABASE_NAME = "MyActivityAppBase.db";
  private static final int VERSION = 1;
  
  public MyActivityBaseHelper(Context paramContext)
  {
    super(paramContext, "MyActivityAppBase.db", null, 1);
  }
  
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("create table my_activities( _id integer primary key autoincrement, uuid, title, date, solved, suspect, type, comment, duration, longitude, latitude)");
    paramSQLiteDatabase.execSQL("create table user_profile( _id integer primary key autoincrement, uuid, name, email, comment, gender, idnum)");
  }
  
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2) {}
}