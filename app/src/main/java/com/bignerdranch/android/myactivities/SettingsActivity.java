package com.bignerdranch.android.myactivities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import java.util.UUID;

public class SettingsActivity
  extends SingleFragmentActivity
{
  private static final String EXTRA_UP_ID = "com.bignerdranch.android.myactivities.up_id";
  
  public static Intent newIntent(Context context, UUID uuid)
  {
    Intent intent = new Intent(context, SettingsActivity.class);
    intent.putExtra("com.bignerdranch.android.myactivities.up_id", uuid);
    return intent;
  }
  
  protected Fragment createFragment()
  {
    return SettingsFragment.newInstance((UUID)getIntent().getSerializableExtra("com.bignerdranch.android.myactivities.up_id"));
  }
}