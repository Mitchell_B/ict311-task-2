package com.bignerdranch.android.myactivities;

import java.util.UUID;

public class UserProfile
{
  private String mComment = "Example comment";
  private String mEmail = "maxbishop@gmail.com";
  private String mGender = "Male";
  private UUID mId;
  private String mIdnum = "107395";
  private String mName = "Max";
  
  public UserProfile(UUID paramUUID)
  {
    setmId(paramUUID);
    setmName("Max Bishop");
    setmEmail("maxbishop@gmail.com");
    setmComment("Example comment");
    setmGender("Male");
    setmIdnum("107395");
  }
  
  public String getmComment()
  {
    return this.mComment;
  }
  
  public String getmEmail()
  {
    return this.mEmail;
  }
  
  public String getmGender()
  {
    return this.mGender;
  }
  
  public UUID getmId()
  {
    return this.mId;
  }
  
  public String getmIdnum()
  {
    return this.mIdnum;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public void setmComment(String paramString)
  {
    this.mComment = paramString;
  }
  
  public void setmEmail(String paramString)
  {
    this.mEmail = paramString;
  }
  
  public void setmGender(String paramString)
  {
    this.mGender = paramString;
  }
  
  public void setmId(UUID paramUUID)
  {
    this.mId = paramUUID;
  }
  
  public void setmIdnum(String paramString)
  {
    this.mIdnum = paramString;
  }
  
  public void setmName(String paramString)
  {
    this.mName = paramString;
  }
}