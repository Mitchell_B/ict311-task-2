package com.bignerdranch.android.myactivities;

import java.util.Date;
import java.util.UUID;

public class MyActivity
{
  private String mComment;
  private Date mDate;
  private String mDuration;
  private UUID mId;
  private String mLatitude;
  private String mLongitude;
  private boolean mSolved;
  private String mSuspect;
  private String mTitle;
  private String mType;
  
  public MyActivity()
  {
    this(UUID.randomUUID());
  }
  
  public MyActivity(UUID paramUUID)
  {
    this.mId = paramUUID;
    this.mDate = new Date();
  }
  
  public String getComment()
  {
    return this.mComment;
  }
  
  public Date getDate()
  {
    return this.mDate;
  }
  
  public String getDuration()
  {
    return this.mDuration;
  }
  
  public UUID getId()
  {
    return this.mId;
  }
  
  public String getLatitude()
  {
    return this.mLatitude;
  }
  
  public String getLongitude()
  {
    return this.mLongitude;
  }
  
  public String getPhotoFilename()
  {
    return "IMG_" + getId().toString() + ".jpg";
  }
  
  public String getSuspect()
  {
    return this.mSuspect;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public boolean isSolved()
  {
    return this.mSolved;
  }
  
  public void setComment(String paramString)
  {
    this.mComment = paramString;
  }
  
  public void setDate(Date paramDate)
  {
    this.mDate = paramDate;
  }
  
  public void setDuration(String paramString)
  {
    this.mDuration = paramString;
  }
  
  public void setLatitude(String paramString)
  {
    this.mLatitude = paramString;
  }
  
  public void setLongitude(String paramString)
  {
    this.mLongitude = paramString;
  }
  
  public void setSuspect(String paramString)
  {
    this.mSuspect = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
  
  public void setType(String paramString)
  {
    this.mType = paramString;
  }
}