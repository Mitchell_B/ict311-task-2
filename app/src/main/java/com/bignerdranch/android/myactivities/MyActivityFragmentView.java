package com.bignerdranch.android.myactivities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import java.io.File;
import java.util.UUID;

public class MyActivityFragmentView
  extends Fragment
{
  private static final String ARG_ACTIVITY_ID = "my_activity_id";
  private static final String EXTRA_MAP_LAT = "com.bignerdranch.android.myactivities.map_lat";
  private static final String EXTRA_MAP_LONG = "com.bignerdranch.android.myactivities.map_long";
  private static final int REQUEST_CONTACT = 1;
  private static final int REQUEST_PHOTO = 2;
  private EditText mComment;
  private Button mDateButton;
  private Button mDeleteButton;
  private EditText mDuration;
  private MyActivity mMyActivity;
  private ImageButton mPhotoButton;
  private File mPhotoFile;
  private ImageView mPhotoView;
  private Button mPlaceButton;
  private String mPlaceButtonString;
  private Button mSaveButton;
  private EditText mTitleField;
  private EditText mTypeText;
  
  public static MyActivityFragmentView newInstance(UUID paramUUID)
  {
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("my_activity_id", paramUUID);
    paramUUID = new MyActivityFragmentView();
    paramUUID.setArguments(localBundle);
    return paramUUID;
  }
  
  private void updatePhotoView()
  {
    if ((this.mPhotoFile == null) || (!this.mPhotoFile.exists()))
    {
      this.mPhotoView.setImageDrawable(null);
      return;
    }
    Bitmap localBitmap = PictureUtils.getScaledBitmap(this.mPhotoFile.getPath(), getMyActivity());
    this.mPhotoView.setImageBitmap(localBitmap);
  }
  
  public MyActivity getMyActivity()
  {
    return this.mMyActivity;
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt2 != -1) {}
    do
    {
      return;
      if ((paramInt1 == 1) && (paramIntent != null))
      {
        paramIntent = paramIntent.getData();
        paramIntent = getMyActivity().getContentResolver().query(paramIntent, new String[] { "display_name" }, null, null, null);
        try
        {
          paramInt1 = paramIntent.getCount();
          if (paramInt1 == 0) {
            return;
          }
          paramIntent.moveToFirst();
          String str = paramIntent.getString(0);
          this.mMyActivity.setSuspect(str);
          return;
        }
        finally
        {
          paramIntent.close();
        }
      }
    } while (paramInt1 != 2);
    updatePhotoView();
  }
  
  public void onCreate(Bundle bundle)
  {
    super.onCreate(bundle);
    UUID uuid = (UUID)getArguments().getSerializable("my_activity_id");
    this.mMyActivity = MyActivityLab.getMyActivity(getActivity()).getMyActivity(uuid);
    this.mPhotoFile = MyActivityLab.getMyActivity(getActivity()).getPhotoFile(this.mMyActivity);
  }
  
  public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, final Bundle bundle)
  {
    boolean bool = true;
    inflater = inflater.inflate(R.layout.fragment_activity, viewGroup, false);
    this.mTypeText = ((EditText)inflater.findViewById(R.id.my_activity_type_text));
    this.mTypeText.setText(this.mMyActivity.getType());
    this.mTypeText.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        MyActivityFragmentView.this.mMyActivity.setType(paramAnonymousCharSequence.toString());
      }
    });
    this.mTitleField = ((EditText)inflater.findViewById(R.id.my_activity_title));
    this.mTitleField.setText(this.mMyActivity.getTitle());
    this.mTitleField.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        MyActivityFragmentView.this.mMyActivity.setTitle(paramAnonymousCharSequence.toString());
      }
    });
    this.mComment = ((EditText)inflater.findViewById(R.id.my_activity_comment));
    this.mComment.setText(this.mMyActivity.getComment());
    this.mComment.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        MyActivityFragmentView.this.mMyActivity.setComment(paramAnonymousCharSequence.toString());
      }
    });
    this.mDuration = ((EditText)inflater.findViewById(R.id.my_activity_duration));
    this.mDuration.setText(this.mMyActivity.getDuration());
    this.mDuration.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        MyActivityFragmentView.this.mMyActivity.setDuration(paramAnonymousCharSequence.toString());
      }
    });
    this.mDateButton = ((Button)inflater.findViewById(R.id.my_activity_date));
    this.mDateButton.setText(this.mMyActivity.getDate().toString());
    this.mDateButton.setEnabled(false);
    this.mSaveButton = ((Button)inflater.findViewById(R.id.my_activity_save));
    this.mSaveButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View view)
      {
        MyActivityLab.getMyActivity(MyActivityFragmentView.this.getActivity()).updateMyActivity(MyActivityFragmentView.this.mMyActivity);
        Intent intent = new Intent(view.getContext(), MyActivityListActivity.class);
        MyActivityFragmentView.this.getActivity().finish();
        MyActivityFragmentView.this.startActivity(intent);
      }
    });
    this.mDeleteButton = ((Button)inflater.findViewById(R.id.my_activity_delete));
    this.mDeleteButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View view)
      {
        MyActivityLab.getMyActivity(MyActivityFragmentView.this.getActivity()).deleteMyActivity(MyActivityFragmentView.this.mMyActivity);
        Intent intent = new Intent(view.getContext(), MyActivityListActivity.class);
        MyActivityFragmentView.this.getActivity().finish();
        MyActivityFragmentView.this.startActivity(intent);
      }
    });
    PackageManager packageManager = getActivity().getPackageManager();
    this.mPhotoButton = ((ImageButton)inflater.findViewById(R.id.my_activity_photo));
    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
    if ((this.mPhotoFile != null) && (intent.resolveActivity(packageManager) != null)) {}
    for (;;)
    {
      this.mPhotoButton.setEnabled(bool);
      if (bool) {
        intent.putExtra("output", FileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + ".com.bignerdranch.android.MyActivitiesApp", this.mPhotoFile));
      }
      this.mPhotoButton.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View view)
        {
          MyActivityFragmentView.this.startActivityForResult(intent, 2);
        }
      });
      this.mPhotoView = ((ImageView)inflater.findViewById(R.layout.fragment_activity));
      updatePhotoView();
      return mPhotoView;
      bool = false;
    }
  }
  
  public void onPause()
  {
    super.onPause();
    MyActivityLab.getMyActivity(getActivity()).updateMyActivity(this.mMyActivity);
  }
}