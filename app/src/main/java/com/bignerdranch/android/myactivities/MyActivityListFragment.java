package com.bignerdranch.android.myactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.UUID;

public class MyActivityListFragment extends Fragment
{
  private ActivityAdapter mAdapter;
  private RecyclerView mActivityRecyclerView;
  private UserProfile mUserProfile;
  
  private void updateUI()
  {
    MyActivityLab myActivityLab = MyActivityLab.getMyActivity(getActivity());
    List list = MyActivityLab.getMyActivity(getActivity()).getMyActivities();
    if (this.mAdapter == null)
    {
      this.mAdapter = new ActivityAdapter(list);
      this.mActivityRecyclerView.setAdapter(this.mAdapter);
      return;
    }
    this.mAdapter.notifyDataSetChanged();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
    if ((ContextCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_FINE_LOCATION") == 0) || (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), "android.permission.ACCESS_FINE_LOCATION"))) {
      return;
    }
    ActivityCompat.requestPermissions(getActivity(), new String[] { "android.permission.ACCESS_FINE_LOCATION" }, 100);
  }
  
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
  {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.fragment_my_activity_list, menu);
  }
  
  public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle)
  {
    inflater = inflater.inflate(R.layout.fragment_activity, viewGroup, false);
    this.mActivityRecyclerView = ((RecyclerView)inflater.findViewById(R.id.my_activity_recycler_view));
    this.mActivityRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    updateUI();
    return viewGroup;
  }
  
  public boolean onOptionsItemSelected(MenuItem menuItem)
  {
    switch (menuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(menuItem);
    case 2131230815: 
      menuItem = new MyActivity();
      MyActivityLab.getMyActivity(getActivity()).addMyActivity(menuItem);
      startActivity(MyActivityActivity.newIntent(getActivity(), menuItem.getId()));
      return true;
    }
    menuItem = MyActivityLab.getMyActivity(getActivity()).getUserProfile(UUID.fromString("4187e5aa-b8cd-11e7-abc4-cec278b6b50a"));
    if (menuItem == null) {}
    for (this.mUserProfile = new UserProfile(UUID.fromString("4187e5aa-b8cd-11e7-abc4-cec278b6b50a"));; this.mUserProfile = menuItem)
    {
      MyActivityLab.getMyActivity(getActivity()).addUserProfile(this.mUserProfile);
      startActivity(SettingsActivity.newIntent(getActivity(), this.mUserProfile.getmId()));
      return true;
    }
  }
  
  public void onResume()
  {
    super.onResume();
    updateUI();
  }
  
  private class ActivityAdapter
    extends RecyclerView.Adapter<ActivityHolder>
  {
    private List<MyActivity> mActivities;
    
    public ActivityAdapter()
    {
      List localList;
      this.mActivities = localList;
    }
    
    public int getItemCount()
    {
      return this.mActivities.size();
    }
    
    public void onBindViewHolder(ActivityHolder paramActivityHolder, int paramInt)
    {
      paramActivityHolder.bindMyActivity((MyActivity)this.mActivities.get(paramInt));
    }
    
    public ActivityHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
    {
      paramViewGroup = LayoutInflater.from(MyActivityListFragment.this.getActivity()).inflate(R.id.my_activity_holder, paramViewGroup, false);
      return new ActivityHolder(MyActivityListFragment.this, paramViewGroup);
    }
  }
  
  private class ActivityHolder
    extends RecyclerView.ViewHolder
    implements View.OnClickListener
  {
    private TextView mDateTextView;
    private MyActivity mMyActivity;
    private TextView mTitleTextView;
    
    public ActivityHolder(View paramView)
    {
      super();
      paramView.setOnClickListener(this);
      this.mTitleTextView = ((TextView)paramView.findViewById(R.id.list_item_activity_title_text_view));
      this.mDateTextView = ((TextView)paramView.findViewById(R.id.list_item_activity_date_text_view));
    }
    
    public void bindMyActivity(MyActivity paramMyActivity)
    {
      this.mMyActivity = paramMyActivity;
      this.mTitleTextView.setText(this.mMyActivity.getTitle());
      this.mDateTextView.setText(this.mMyActivity.getDate().toString());
    }
    
    public void onClick(View v)
    {
      Intent intent = MyActivityActivityView.newIntent(MyActivityListFragment.this.getActivity(), this.mMyActivity.getId());
      MyActivityListFragment.this.startActivity(intent);
    }
  }
}