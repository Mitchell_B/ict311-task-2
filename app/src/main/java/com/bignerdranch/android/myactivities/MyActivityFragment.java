package com.bignerdranch.android.myactivities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.UUID;

public class MyActivityFragment
  extends Fragment
{
  private static final String ARG_ACTIVITY_ID = "my_activity_id";
  private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 100;
  private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1001;
  private static final int REQUEST_CONTACT = 1;
  private static final int REQUEST_PHOTO = 2;
  private static final String TAG = "LocateFragment";
  private Button mCancelButton;
  private EditText mComment;
  private Button mDateButton;
  private EditText mDuration;
  private MyActivity mMyActivity;
  private ImageButton mPhotoButton;
  private File mPhotoFile;
  private ImageView mPhotoView;
  private Button mPlaceButton;
  private String mPlaceButtonString;
  private Button mSaveButton;
  private EditText mTitleField;

  
  public static MyActivityFragment newInstance(UUID paramUUID)
  {
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("my_activity_id", paramUUID);
    MyActivityFragment fragment = new MyActivityFragment();
    fragment.setArguments(localBundle);
    return fragment;
  }
  
  private void updatePhotoView()
  {
    if ((this.mPhotoFile == null) || (!this.mPhotoFile.exists()))
    {
      this.mPhotoView.setImageDrawable(null);
      return;
    }
    Bitmap localBitmap = PictureUtils.getScaledBitmap(this.mPhotoFile.getPath(), getActivity());
    this.mPhotoView.setImageBitmap(localBitmap);
  }
  
  public MyActivity getMyActivity()
  {
    return this.mMyActivity;
  }

  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle onSavedInstanceState)
  {
    boolean bool2 = false;
    View v = inflater.inflate(R.layout.fragment_activity, container, false);
    ViewGroup c = (Spinner)inflater.findViewById(R.layout.fragment_activity);

    container.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
    {
      public void onItemSelected(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        MyActivityFragment.this.mMyActivity.setType(paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt).toString());
      }
      
      public void onNothingSelected(AdapterView<?> paramAnonymousAdapterView) {}
    });
    ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(inflater.getContext(), 2130837504, 17367048);
    arrayAdapter.setDropDownViewResource(R.layout.fragment_my_activity);
    container.setAdapter(savedInstanceState);
    this.mTitleField = ((EditText)inflater.findViewById(R.id.my_activity_title));
    this.mTitleField.setText(this.mMyActivity.getTitle());
    this.mTitleField.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        MyActivityFragment.this.mMyActivity.setTitle(paramAnonymousCharSequence.toString());
      }
    });
    this.mComment = ((EditText)inflater.findViewById(R.id.my_activity_comment));
    this.mComment.setText(this.mMyActivity.getComment());
    this.mComment.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        MyActivityFragment.this.mMyActivity.setComment(paramAnonymousCharSequence.toString());
      }
    });
    this.mDuration = ((EditText)inflater.findViewById(R.id.my_activity_duration));
    this.mDuration.setText(this.mMyActivity.getDuration());
    this.mDuration.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        MyActivityFragment.this.mMyActivity.setDuration(paramAnonymousCharSequence.toString());
      }
    });
    this.mDateButton = ((Button)inflater.findViewById(R.id.my_activity_date));
    this.mDateButton.setText(this.mMyActivity.getDate().toString());
    this.mDateButton.setEnabled(false);
    this.mSaveButton = ((Button)inflater.findViewById(R.id.my_activity_save));
    this.mSaveButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View view)
      {
        MyActivityLab.getMyActivity(MyActivityFragment.this.getActivity()).updateMyActivity(MyActivityFragment.this.mMyActivity);
        Intent intent = new Intent(view.getContext(), MyActivityListActivity.class);
        MyActivityFragment.this.getActivity().finish();
        MyActivityFragment.this.startActivity(intent);
      }
    });
    this.mCancelButton = ((Button)inflater.findViewById(R.id.my_activity_cancel));
    this.mCancelButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View view)
      {
        MyActivityLab.getMyActivity(MyActivityFragment.this.getActivity()).deleteMyActivity(MyActivityFragment.this.mMyActivity);
        Intent intent = new Intent(view.getContext(), MyActivityListActivity.class);
        MyActivityFragment.this.getActivity().finish();
        MyActivityFragment.this.startActivity(intent);
      }
    });
    PackageManager packageManager = getActivity().getPackageManager();
    this.mPhotoButton = ((ImageButton)inflater.findViewById(R.id.my_activity_photo));
    final Intent captureImage = new Intent("android.media.action.IMAGE_CAPTURE");
    boolean bool1 = bool2;
    if (this.mPhotoFile != null)
    {
      bool1 = bool2;
      if (captureImage.resolveActivity(packageManager) != null) {
        bool1 = true;
      }
    }
    this.mPhotoButton.setEnabled(bool1);
    if (bool1)
    {
      captureImage.putExtra("output", FileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + ".com.bignerdranch.android.MyActivitiesApp", this.mPhotoFile));
    }
    this.mPhotoButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View view)
      {
        MyActivityFragment.this.startActivityForResult(captureImage, REQUEST_PHOTO);
      }
    });
    this.mPhotoView = ((ImageView)inflater.findViewById(R.id.my_activity_photo));
    updatePhotoView();
    return mPhotoView;
  }
  
  public void onPause()
  {
    super.onPause();
    MyActivityLab.getActivity(getMyActivity()).updateActivity(this.mMyActivity);
  }
  
  public void onResume()
  {
    super.onResume();
  }
}