package com.bignerdranch.android.myactivities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import java.util.UUID;

public class MyActivityActivityView
  extends SingleFragmentActivity
{
  private static final String EXTRA_ACTIVITY_ID = "com.bignerdranch.android.myactivities.my_activity_id";
  
  public static Intent newIntent(Context paramContext, UUID paramUUID)
  {
    Intent intent = new Intent(paramContext, MyActivityActivityView.class);
    intent.putExtra(EXTRA_ACTIVITY_ID, paramUUID);
    return intent;
  }
  
  protected Fragment createFragment()
  {
    return MyActivityFragmentView.newInstance((UUID)getIntent().getSerializableExtra("com.bignerdranch.android.myactivities.my_activity_id"));
  }
}