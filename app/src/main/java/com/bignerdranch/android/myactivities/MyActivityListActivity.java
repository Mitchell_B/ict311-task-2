package com.bignerdranch.android.myactivities;

import android.support.v4.app.Fragment;

public class MyActivityListActivity
  extends SingleFragmentActivity
{
  protected Fragment createFragment()
  {
    return new MyActivityListFragment();
  }
}