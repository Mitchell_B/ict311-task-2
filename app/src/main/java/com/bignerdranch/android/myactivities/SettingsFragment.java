package com.bignerdranch.android.myactivities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import java.util.UUID;

public class SettingsFragment
  extends Fragment
{
  private static final String ARG_UP_ID = "Up_id";
  private EditText mTextComment;
  private EditText mTextEmail;
  private EditText mTextGender;
  private EditText mTextID;
  private EditText mTextName;
  private UserProfile mUp;
  
  public static SettingsFragment newInstance(UUID uuid)
  {
    Bundle bundle = new Bundle();
    bundle.putSerializable(ARG_UP_ID, uuid);
    Fragment settingsFragment = new SettingsFragment();
    bundle.setArguments(settingsFragment);
    return bundle;
  }
  
  public void onCreate(Bundle bundle)
  {
    super.onCreate(bundle);
    UUID uuid = (UUID)getArguments().getSerializable(ARG_UP_ID);
    this.mUp = MyActivityLab.getMyActivity(getActivity()).getUserProfile(uuid);
  }
  
  public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle)
  {
    inflater = inflater.inflate(R.layout.fragment_activity, viewGroup, false);
    this.mTextName = ((EditText)inflater.findViewById(R.id.my_activities_name));
    this.mTextName.setText(this.mUp.getmName());
    this.mTextName.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        SettingsFragment.this.mUp.setmName(paramAnonymousCharSequence.toString());
      }
    });
    this.mTextEmail = ((EditText)inflater.findViewById(R.id.my_activities_email));
    this.mTextEmail.setText(this.mUp.getmEmail());
    this.mTextEmail.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        SettingsFragment.this.mUp.setmEmail(paramAnonymousCharSequence.toString());
      }
    });
    this.mTextComment = ((EditText)inflater.findViewById(R.id.my_activities_comment));
    this.mTextComment.setText(this.mUp.getmComment());
    this.mTextComment.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        SettingsFragment.this.mUp.setmComment(paramAnonymousCharSequence.toString());
      }
    });
    this.mTextGender = ((EditText)inflater.findViewById(R.id.my_activities_gender));
    this.mTextGender.setText(this.mUp.getmGender());
    this.mTextGender.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        SettingsFragment.this.mUp.setmGender(paramAnonymousCharSequence.toString());
      }
    });
    this.mTextID = ((EditText)inflater.findViewById(R.id.my_activities_id));
    this.mTextID.setText(this.mUp.getmIdnum());
    this.mTextID.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable) {}
      
      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
      
      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
        SettingsFragment.this.mUp.setmIdnum(paramAnonymousCharSequence.toString());
      }
    });
    return viewGroup;
  }
  
  public void onPause()
  {
    super.onPause();
    MyActivityLab.getMyActivity(getActivity()).updateUserProfile(this.mUp);
  }
}