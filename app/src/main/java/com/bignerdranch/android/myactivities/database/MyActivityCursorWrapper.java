package com.bignerdranch.android.myactivities.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import com.bignerdranch.android.myactivities.MyActivity;
import com.bignerdranch.android.myactivities.UserProfile;
import java.util.Date;
import java.util.UUID;

public class MyActivityCursorWrapper
  extends CursorWrapper
{
  public MyActivityCursorWrapper(Cursor paramCursor)
  {
    super(paramCursor);
  }
  
  public MyActivity getMyActivities()
  {
    Object localObject = getString(getColumnIndex("uuid"));
    String str1 = getString(getColumnIndex("title"));
    long l = getLong(getColumnIndex("date"));
    int i = getInt(getColumnIndex("solved"));
    String str2 = getString(getColumnIndex("suspect"));
    String str3 = getString(getColumnIndex("type"));
    String str4 = getString(getColumnIndex("duration"));
    String str5 = getString(getColumnIndex("comment"));
    String str6 = getString(getColumnIndex("latitude"));
    String str7 = getString(getColumnIndex("longitude"));
    localObject = new MyActivity(UUID.fromString((String)localObject));
    ((MyActivity)localObject).setTitle(str1);
    ((MyActivity)localObject).setDate(new Date(l));
    if (i != 0) {}
    for (boolean bool = true;; bool = false)
    {
      ((MyActivity)localObject).setSuspect(str2);
      ((MyActivity)localObject).setType(str3);
      ((MyActivity)localObject).setComment(str5);
      ((MyActivity)localObject).setDuration(str4);
      ((MyActivity)localObject).setLatitude(str6);
      ((MyActivity)localObject).setLongitude(str7);
      return (MyActivity)localObject;
    }
  }
  
  public UserProfile getUserProfile()
  {
    Object localObject = getString(getColumnIndex("uuid"));
    String str1 = getString(getColumnIndex("name"));
    String str2 = getString(getColumnIndex("email"));
    String str3 = getString(getColumnIndex("comment"));
    String str4 = getString(getColumnIndex("gender"));
    String str5 = getString(getColumnIndex("idnum"));
    localObject = new UserProfile(UUID.fromString((String)localObject));
    ((UserProfile)localObject).setmName(str1);
    ((UserProfile)localObject).setmEmail(str2);
    ((UserProfile)localObject).setmComment(str3);
    ((UserProfile)localObject).setmGender(str4);
    ((UserProfile)localObject).setmIdnum(str5);
    return (UserProfile)localObject;
  }
}